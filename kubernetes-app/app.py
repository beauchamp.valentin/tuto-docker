# kubernetes-app/app.py
import time
import os

from flask import Flask, json, request
from flask_redis_sentinel import SentinelExtension
from http import HTTPStatus
from werkzeug.exceptions import HTTPException

from threading import Thread
from prometheus_client import start_http_server, Counter


redis_sentinel = SentinelExtension()
redis_connection = redis_sentinel.default_connection

app = Flask(__name__)
app.config['REDIS_URL'] = "redis+sentinel://%s:26379/mymaster/0" % os.getenv('REDIS_SENTINEL_SERVICE')
redis_sentinel.init_app(app)

num_requests = Counter("num_requests", "Example counter")

def compute():
    app.logger.info("Starting CPU-consuming thread")
    timeNow = int(round(time.time()))
    timeToStop = timeNow + 200
    while timeToStop > timeNow:
        99 * 99
        timeNow = int(round(time.time()))
    app.logger.info("Ending CPU-consuming thread")

@app.route("/")
def hello():
    app.logger.info("This is a log line into the default route")
    num_requests.inc()
    return "Hello Kubernetes!"

@app.route("/config")
def config():
    num_requests.inc()
    with open('/etc/config/message','r') as f:
        return f.read()

@app.route('/messages/<message>', methods=['GET'])          
def get_message(message):                                   
    m = redis_sentinel.master_for("mymaster").get(message)  
    if m is None:                                           
        return "Not Found", HTTPStatus.NOT_FOUND            
    return m.decode("utf-8")                                


@app.route('/messages/<message>', methods=['PUT'])                                                           
def put_message(message):                                                                                    
    if request.headers['Content-Type'] != 'application/json':                                                
        return "HTTP header 'Content-Type: application/json' expected", HTTPStatus.UNSUPPORTED_MEDIA_TYPE    
    redis_sentinel.master_for("mymaster").set(message, request.data)                                         
    return "Created", HTTPStatus.CREATED                                                                     


@app.route('/messages/<message>', methods=['DELETE'])                         
def delete_message(message):                                                  
    if redis_sentinel.master_for("mymaster").delete(message) == 1:            
        return "Deleted", HTTPStatus.NO_CONTENT

@app.route("/healthz")
def healthz():
    return "Tutto bene !"

@app.route('/ready')                                                                       
def ready():                                                                               
    try:                                                                                   
        if redis_sentinel.master_for("mymaster").ping():                                   
            return "PING OK"                                                               
        raise Exception("PING KO")                                                         
    except Exception as exception:                                                         
        app.logger.error("Exception: %s" % exception)                                      
        return "Redis master server unavailable", HTTPStatus.INTERNAL_SERVER_ERROR

@app.route("/slow")
def slow():
    t = Thread(target=compute)
    t.start()
    return "CPU is going to heat"

@app.errorhandler(HTTPException)
def handle_http_exception(exception: HTTPException):
    app.logger.error("Raised Exception: %s" % exception)
    return exception.description, exception.code

@app.errorhandler(Exception)
def handle_exception(exception: Exception):
    app.logger.error("Raised Exception: %s" % exception)
    return HTTPStatus.INTERNAL_SERVER_ERROR.description, HTTPStatus.INTERNAL_SERVER_ERROR

start_http_server(9001)
